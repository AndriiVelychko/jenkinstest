package Pets;

import java.util.List;

public class Pet {
    private int id;
    private Category category;
    private String name;
    private List photoUrl;
    private Status status;
    private Tags[] tags;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(List photoUrl) {
        this.photoUrl = photoUrl;
    }
    public Pet(int id, Category category, String name, List<String> photoUrl, Status status, Tags[] tags) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.photoUrl = photoUrl;
        this.status = status;
        this.tags = tags;
    }
}
