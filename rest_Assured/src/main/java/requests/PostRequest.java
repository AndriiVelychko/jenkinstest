package requests;

import Constants.EndPoints;
import Pets.Category;
import Pets.Pet;
import Pets.Status;
import Pets.Tags;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class PostRequest {
    public static final String BASE_URI = "https://petstore.swagger.io/";

    public Response postResponse(String endPoint,String requestBody) {

        return given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .when()
                .post(BASE_URI + endPoint)
                .then()
                .extract().response();
    }


    public Response postResponseToUpdate(int id, String requestBody) {

        return given()
                .contentType(ContentType.URLENC)
                .body(requestBody)
                .when()
                .post(BASE_URI + EndPoints.findPetsById + id)
                .then()
                .extract().response();
    }

    public Response postResponseWithImage(int id, String additionalData, String filePath) {
        return given()
                .contentType("multipart/form-data")
                .multiPart("additionalMetadata", additionalData)
                .multiPart("file", new File(filePath))
                .when()
                .post(BASE_URI + EndPoints.findPetsById + id + "/uploadImage")
                .then()
                .extract().response();
    }

    public String getDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return result.toString();
    }
}



