package requests;

import Constants.EndPoints;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class PutRequest {
    public static final String BASE_URI = "https://petstore.swagger.io/";

    public Response putResponse( String requestBody) {

        return given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .when()
                .put(BASE_URI + EndPoints.findPetsById)
                .then()
                .extract().response();
    }
}
