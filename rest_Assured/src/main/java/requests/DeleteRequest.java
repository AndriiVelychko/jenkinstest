package requests;

import Constants.EndPoints;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;

import static io.restassured.RestAssured.given;

public class DeleteRequest {
    public static final String BASE_URI = "https://petstore.swagger.io/";

    public Response deleteResponse(String getPart) {
        Response response = given()
                .baseUri(BASE_URI)
                .header("Content-type", "application/json")
                .when()
                .delete(getPart)
                .then()
                .extract().response();
        return response;
    }

    public Response deletePetById(int id) {
        return deleteResponse(EndPoints.findPetsById + id);
    }

    public Response deleteOrderById(int id) {
        return deleteResponse(EndPoints.findOrderById + id);
    }

}
