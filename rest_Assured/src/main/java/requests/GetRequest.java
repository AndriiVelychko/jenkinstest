package requests;
import io.restassured.http.ContentType;
import Constants.EndPoints;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class GetRequest {
    public static final String BASE_URI = "https://petstore.swagger.io/";

    public Response getResponse(String getPart) {
        Response response = given()
                .contentType(ContentType.JSON)
                .when()
                .get(BASE_URI + getPart)
                .then()
                .extract().response();
        return response;
    }

    public Response getByStatus(String status) {
        return getResponse(EndPoints.findPetsByStatus + status);
    }

    public Response getById(int id) {
        return getResponse(EndPoints.findPetsById + id);
    }

    public Response getStoreInv() {
        return getResponse(EndPoints.findInventory);
    }

    public Response getOrder(int id) {
        return getResponse(EndPoints.findOrderById + id);
    }
}
