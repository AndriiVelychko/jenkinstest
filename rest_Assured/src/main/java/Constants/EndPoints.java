package Constants;

public class EndPoints {
    public static final String findPetsByStatus="v2/pet/findByStatus?status=";
    public static final String findPetsById ="v2/pet/";
    public static final String findInventory="v2/store/inventory";
    public static final String findOrderById="v2/store/order/";

}
