package PetTests;

import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import requests.DeleteRequest;

public class DeleteTest {
    DeleteRequest deleteRequest = new DeleteRequest();

    @Test
    public void shouldDeletePet() {
        int id = 1499;
        Response response = deleteRequest.deletePetById(id);
        if (response.statusCode() == 200) {
            Assert.assertEquals(response.jsonPath().getString("message"), String.valueOf(id));
        } else {
            Assert.assertTrue(response.statusCode()==404);
        }
    }


}
