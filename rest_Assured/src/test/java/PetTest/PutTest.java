package PetTests;

import Pets.Category;
import Pets.Pet;
import Pets.Status;
import Pets.Tags;
import com.google.gson.Gson;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import requests.PutRequest;
import PetTests.PostTest.*;

import java.util.ArrayList;
import java.util.List;

public class PutTest {
    PutRequest putRequest = new PutRequest();

    @Test
    public void shouldUpdate() {
        Pet pet = createPetDto();
        String json = new Gson().toJson(pet);
        Response response = putRequest.putResponse(json);
        String expectedName = "Antoooooon";
        Assert.assertTrue(response.jsonPath().getString("name").equals(expectedName));
    }

    public Pet createPetDto() {
        Category category = new Category(661, "cats");
        Tags tags = new Tags(155, "wwwwwwwww");
        Tags[] array = {tags};
        List<String> urlsPh = new ArrayList<>();
        urlsPh.add("url1");
        urlsPh.add("url2");
        return new Pet(1499, category, "Antoooooon", urlsPh, Status.SOLD, array);

    }
}
