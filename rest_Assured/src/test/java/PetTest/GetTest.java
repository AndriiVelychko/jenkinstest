package PetTests;

import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import requests.GetRequest;

import static io.restassured.RestAssured.given;

public class GetTest {
    GetRequest getRequest = new GetRequest();

    @Test
    public void shouldReturnTrueIfTheresPetWith(){
        Response response = getRequest.getByStatus("available");
        String name = "doggie";
        Assert.assertTrue(response.jsonPath().getList("name").contains(name));
    }
    @Test
    public void shouldReturnPetById(){
        Response response = getRequest.getById(1499);
        String name = "Antoooooon";
		String expectedMessage = "Pet not found";
        if (response.statusCode() == 404) {
            Assert.assertEquals(expectedMessage, response.jsonPath().getString("message"));
        } else {
            Assert.assertEquals(name, response.jsonPath().getString("name"));
        }
    }


}
