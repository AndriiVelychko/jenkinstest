package PetTests;

import Constants.EndPoints;
import Pets.Category;
import Pets.Pet;
import Pets.Status;
import Pets.Tags;
import com.google.gson.Gson;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import requests.PostRequest;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PostTest  {
    PostRequest postRequest = new PostRequest();

    @Test
    public void shouldPostPet(){
        Pet pet = createPetDto();
        String json = new Gson().toJson(pet);
        Response response= postRequest.postResponse(EndPoints.findOrderById,json);
        Assert.assertTrue(pet.getId()==response.jsonPath().getInt("id"));
    }
    @Test
    public void shouldUpdatePetWithPost() throws UnsupportedEncodingException {
        HashMap<String,String> pets= new HashMap<>();
        pets.put("id", "10");
        pets.put("name", "Anton");
        pets.put("status", "pending");
        String data = postRequest.getDataString(pets);
        Response response= postRequest.postResponseToUpdate(10,data);
        Assert.assertTrue(response.statusCode()==200);
    }
    @Test
    public void shouldCreatePetWithImage(){
        Response response = postRequest.postResponseWithImage(8,"ddd" , "src/main/resources/photo_2021-01-09_14-45-14.jpg");
    }

    public Pet createPetDto() {
        Category category = new Category(661, "cats");
        Tags tags = new Tags(155, "wwwwwwwww");
        Tags[] array = {tags};
        List<String> urlsPh = new ArrayList<>();
        urlsPh.add("url1");
        urlsPh.add("url2");
        return new Pet(1499, category, "Antin", urlsPh,Status.SOLD, array);

    }

}
