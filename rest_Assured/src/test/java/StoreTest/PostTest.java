package StoreTests;

import Constants.EndPoints;
import com.google.gson.Gson;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import requests.PostRequest;
import store.Order;
import store.Status;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PostTest {
    PostRequest request = new PostRequest();
    @Test
    public void shouldCreateNewOrder(){
        Order order = createOrderDto();
        String json = new Gson().toJson(order);
        Response response = request.postResponse(EndPoints.findOrderById,json);
        Assert.assertTrue(response.jsonPath().getInt("id")==createOrderDto().getId());
    }
    private Order createOrderDto(){
        SimpleDateFormat dd = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        Date date = new Date();
        String date1 = dd.format(date);
        return new Order(4, 13, 5, date1, Status.APPROVED, false);
    }
}
