package StoreTests;

import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import requests.GetRequest;

public class GetTest {
    GetRequest getRequest = new GetRequest();

    @Test
    public void shouldReturnFromStoreInv() {
        Response response = getRequest.getStoreInv();
        Assert.assertTrue(response.jsonPath().getInt("available") > 0);
    }

    @Test
    public void shouldReturnOrderFromStore() {
        int id = 4;
        Response response = getRequest.getOrder(id);
        if (response.statusCode() == 404) {
            Assert.assertTrue(response.jsonPath().getString("message").contains("not found"));
        } else {
            Assert.assertTrue(id == response.jsonPath().getInt("id"));
        }
    }
}
