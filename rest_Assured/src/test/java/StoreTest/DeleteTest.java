package StoreTests;

import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import requests.DeleteRequest;

public class DeleteTest {
    DeleteRequest deleteRequest = new DeleteRequest();

    @Test
    public void shouldDeleteOrder() {
        int id = 4;
        Response response = deleteRequest.deleteOrderById(id);
        if (response.statusCode() == 404) {
            Assert.assertEquals(response.jsonPath().getString("message"), "Order Not Found");
        } else {
            Assert.assertEquals(response.jsonPath().getString("message"), String.valueOf(id));
        }

    }
}
